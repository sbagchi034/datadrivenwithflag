package org;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;

public class FileUtils {

    public File getLatestFile()
    {

        File dir = new File("C:\\Users\\sbagchi034\\IdeaProjects\\ReadFromExcel\\src\\test\\java\\Resources");

        FileFilter filter = new WildcardFileFilter("*.*");
        File[] listOfFiles = dir.listFiles(filter);
        Arrays.sort(listOfFiles, LastModifiedFileComparator.LASTMODIFIED_REVERSE);

        return listOfFiles[0];
    }
}
