package org;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class ExcelInteraction {

    Sheet sh;


    public ExcelInteraction() {
        File testDataFile = new File("C:\\Users\\sbagchi034\\IdeaProjects\\ReadFromExcel\\src\\test\\java\\Resources\\testData.xlsx");
        Workbook wb = null;
        try {
            wb = WorkbookFactory.create(testDataFile);
        } catch (Exception e) {

        }
        sh = wb.getSheet("Data");

    }

    public HashMap<String, String> hashMapData(int row) {
        HashMap<String, String> hm = new HashMap<>();
        for (int i = 0; i < sh.getRow(0).getLastCellNum(); i++) {
            sh.getRow(row).getCell(i).setCellType(CellType.STRING);
            hm.put(sh.getRow(0).getCell(i).toString(), sh.getRow(row).getCell(i).toString());
        }

        return hm;
    }

    public int getLastRowCount() {
        return sh.getLastRowNum();
    }

    public int getLastRowCountForYes() {
        int count = 0;
        for (int i = 0; i <= sh.getLastRowNum(); i++) {

            if (sh.getRow(i).getCell(1).toString().equalsIgnoreCase("Yes")) {
                count++;
            }

        }
        return count;
    }


}
