package org;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Demo {

    static int a = 0;

    @Test(dataProvider = "getData")
    public void readFromExcel(Object obj) throws IOException {

        FileUtils files = new FileUtils();
        a = a + 1;
        HashMap<String, String> hm = (HashMap<String, String>) obj;
        System.out.println("Test" + a);
        System.out.println(hm);
        //System.out.println(files.getLatestFile().getName());
        //files.getLatestFile().renameTo(new File("C:\\Users\\sbagchi034\\IdeaProjects\\ReadFromExcel\\src\\test\\java\\Resources\\"+hm.get("TC_ID")+"_Renamed.txt"));
        //System.out.println(files.getLatestFile().getName());
    }

    @DataProvider(name = "getData")
    public Object[][] dataProviderMethod() {
        ArrayList list = new ArrayList();
        ExcelInteraction ei = new ExcelInteraction();
        Object[][] obj = new Object[ei.getLastRowCount()][1];
        Object[][] obj2 = new Object[ei.getLastRowCountForYes()][1];
        for (int i = 1; i <= ei.getLastRowCount(); i++) {
            HashMap<String, String> hm = ei.hashMapData(i);
            if (hm.containsValue("Yes")) {
                obj[i - 1][0] = hm;
                list.add(obj[i - 1][0]);
            }
        }
        for (int i = 0; i < list.size(); i++) {
            obj2[i][0] = list.get(i);
        }
        return obj2;
    }

}
